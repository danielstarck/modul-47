import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Palindrome {
    @GeneratedValue
    @Id
    private long id;

    @NotNull
    private String string;

	public long getId() {
        return id;
    }
	
	public void setString(String string) {
		this.string = string;
	}

    public String getString() {
        return string;
    }
}

