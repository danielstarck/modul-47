import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class PalindromeService {

	@PersistenceContext(unitName = "webapp")
	private EntityManager entityManager;
	
	private final Pattern pattern = Pattern.compile("[a-z0-9]+");

	public List<Palindrome> getPalindromes() {
		Query query = entityManager.createQuery("SELECT palindrome FROM Palindrome palindrome");
		List<Palindrome> palindromes = query.getResultList();
		return palindromes;
	}

	public boolean addPalindrome(String string) {
		if (isPalindrome(string)) {
			Palindrome palindrome = new Palindrome();
			palindrome.setString(string);
			entityManager.persist(palindrome);
			return true;
		} else {
			return false;
		}
	}

	private boolean isPalindrome(String string) {
		Matcher matcher = pattern.matcher(string.toLowerCase());
		StringBuilder stringBuilder = new StringBuilder();
		while (matcher.find()) {
			stringBuilder.append(matcher.group());
		}
		System.out.println(stringBuilder.toString());
		return stringBuilder.toString().equals(stringBuilder.reverse().toString());
	}

}
