import java.io.BufferedReader;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet("/")
public class PalindromeServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private PalindromeService palindromeService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		response.getWriter().println(getJSONArray().toString(2));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		StringBuffer stringBuffer = new StringBuffer();
		BufferedReader reader = request.getReader();
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				stringBuffer.append(line).append('\n');
			}
		} finally {
			reader.close();
		}
		JSONObject potentialPalindrome = new JSONObject(stringBuffer.toString());
		boolean addedPalindrome = palindromeService.addPalindrome(potentialPalindrome.getString("string"));
		System.out.println(addedPalindrome ? "palindrome accepted" : "not a palindrome");
	}
	
	private JSONArray getJSONArray() {
		JSONArray jsonArray = new JSONArray();
		palindromeService.getPalindromes().forEach(p -> jsonArray.put(getJSON(p)));
		return jsonArray;
	}
	
	private JSONObject getJSON(Palindrome palindrome) {
		JSONObject jsonPalindrome = new JSONObject();
		jsonPalindrome.put("string", palindrome.getString());
		jsonPalindrome.put("id", palindrome.getId());
		return jsonPalindrome;
	}
	
	
	
}
